var gulp = require('gulp');
var webserver = require('gulp-webserver');
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');
var plumber = require('gulp-plumber');
var rename = require('gulp-rename');
var connect = require('connect');


gulp.task('serve', ['watch'], function() {
    gulp.src('.')
        .pipe(webserver({
            fallback: "index.html",
            port: 3000
        }));

    browserSync.init({
        server: "."
    });

    gulp.watch(['js/*', 'js/**/*', 'templates/*', 'index.html', "style/*", '!js/app-concat.min.js']).on('change', browserSync.reload);

});

gulp.task('watch', ['init'], function () {


    gulp.watch(['js/*', 'js/**/*', '!js/app-concat.min.js'], ['init']);
});

gulp.task('init', function () {
    gulp.src([
        'js/*',
        'js/**/*',
        '!js/app-concat.min.js'
    ])
        .pipe(plumber())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(concat('app-concat.min.js'))
        .pipe(plumber.stop())
        .pipe(gulp.dest('js/'));
});

gulp.task('default', ['serve']);