angular
    .module('genericApp')

    /**
     * @ngdoc service
     * @name genericApp.factory:RestService
     * @function
     *
     * @requires $http
     */
    .factory("RestService", function ($http) {
        return {
            /**
             * @ngdoc method
             * @name genericApp.factory:RestService#submit
             * @methodOf genericApp.factory:RestService
             *
             * @param {string} params               Requests params
             *
             * @description                         Request for get videos list
             * @return {object}                     requests result
             */
            submit: function (params) {
                return $http.post("/submit", params);
            }
        }
    });