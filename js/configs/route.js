angular
    .module('genericApp')
    .config(routerConfig);

/**
 * @ngInject
 * @ngdoc overview
 * @name genericApp.run:routerConfig
 * @description
 * # Route Config
 * Config with route for app
 *
 * @requires $routeProvider
 * @requires $locationProvider
 */
function routerConfig($routeProvider, $locationProvider) {

    $routeProvider
        .when('/', {
            templateUrl: 'templates/post.html',
            controller: 'PostCtrl',
            controllerAs: 'postCtrl'
        })
        .when('/enforcement', {
            templateUrl: 'templates/enforcement.html',
            controller: 'EnforcementCtrl',
            controllerAs: 'enforcementCtrl'
        })
        .otherwise({
            redirectTo: '/enforcement'
        });

    // $locationProvider.html5Mode(true);
}
