var genericApp = angular.module("genericApp");

genericApp.controller("EnforcementCtrl", function (RestService, fCsv, $http, $parse) {

    var vm = this;
    vm.isShowSearch = true;
    vm.selectedArray = [];
    vm.showAccInfo = true;
//
    var csvList;

    // $http.get('/sample.csv').then(function(resp) {
    //     var arr = fCsv.toJson(resp.data);
    //     var v = $parse(arr);
    //     csvList = v(vm);
    //     vm.paginationList(0, 10);
    // });

    vm.paginationList = function (from, to) {
        vm.csvList = (csvList || []).slice(from, to);
    };

    var getPropertiesName = function () {
        vm.propertiesName = [
            {id: 1, name: "field1"},
            {id: 2, name: "field1"},
            {id: 3, name: "field1"},
            {id: 4, name: "field1"},
            {id: 5, name: "field1"},
            {id: 6, name: "field1"},
            {id: 7, name: "field1"},
            {id: 8, name: "field1"},
            {id: 9, name: "field1"},
            {id: 10, name: "field1"},
            {id: 11, name: "field1"},
            {id: 12, name: "field1"},
            {id: 13, name: "field1"},
            {id: 14, name: "field1"}
        ];
    };
    getPropertiesName();
//

    vm.datepickerProperties = {
        isOpen1: false,
        isOpen2: false,

        setIsOpen: function (datepickerIndex) {
            this["isOpen" + datepickerIndex] = !this["isOpen" + datepickerIndex];
        }
    };

    vm.genericList = [];

    for (var i = 0; i < 50; i++){
        vm.genericList.push({
            brand: "brand " + i,
            platform: "platform " + i,
            profileName: "profileName " + i,
            profileId: "profileId " + i,
            profileURL: "profileURL " + i,
            profileFollowers: "profileFollowers " + i,
            profileViews: "profileViews " + i,
            profileLikes: "profileLikes " + i,
            listingId: "listingId " + i,
            listingURL: "listingURL " + i,
            evidenceURL: "evidenceURL " + i,
            listingTitle: "listingTitle " + i,
            listingDescription: "listingDescription " + i,
            listingImageURL: "listingImageURL " + i,
            detectionDate: "detectionDate " + i,
            postLikes: "postLikes " + i,
            postFollowers: "postFollowers " + i,
            postViews: "postViews " + i,
            connectedTo: "connectedTo " + i,
            product: "product " + i,
            country: "country " + i,
            markOfContent: "markOfContent " + i,
            infringementType: "infringementType " + i,
            enforcementStatus: "enforcementStatus " + i,
            escalationStatus: "escalationStatus " + i,
            reportedDate: "reportedDate " + i,
            removedDate: "removedDate " + i,
            accountStatus: "accountStatus " + i,
            submittedBy: "submittedBy " + i,
            succesfull: false,
            comments: "comments " + i
        });
    }

    vm.checkAll = function () {
        vm.selectedArray = [];
        if(vm.isAllChecked) {
            vm.isShowDetailsBox = true;
            angular.forEach(vm.genericList, function (value, index) {
                vm.selectedArray.push(index);
                value.isChecked = vm.isAllChecked;
            })
        } else {
            angular.forEach(vm.genericList, function (value) {
                value.isChecked = vm.isAllChecked;
            })
        }
    };

    vm.checkOne = function (isChecked, index) {
        if(isChecked) {
            vm.isShowDetailsBox = true;
            vm.selectedArray.push(index);
        } else {
            vm.selectedArray.splice(vm.selectedArray.indexOf(index), 1);
        }
    };

});