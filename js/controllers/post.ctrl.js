var genericApp = angular.module("genericApp");

genericApp.controller("PostCtrl", function ($scope, RestService) {

    var vm = this;

    var defaultValue = {
        brand: null,
        platform: null,
        profileName: "",
        profileId: "",
        profileURL: "",
        profileFollowers: null,
        profileViews: null,
        profileLikes: null,
        listingId: "",
        listingURL: "",
        evidenceURL: "",
        listingTitle: "",
        listingDescription: "",
        listingImageURL: "",
        detectionDate: null,
        postLikes: null,
        postFollowers: null,
        postViews: null,
        connectedTo: "",
        product: [],
        country: null,
        markOfContent: null,
        infringementType: null,
        enforcementStatus: null,
        escalationStatus: null,
        reportedDate: null,
        removedDate: null,
        accountStatus: null,
        submittedBy: "",
        succesfull: false,
        comments: ""
    };
    vm.postData = angular.copy(defaultValue);

    vm.datepickerOptions = {
        open1: false,
        open2: false,
        open3: false,

        open: function (name) {
            this[name] = true;
        }
    };

    vm.dropdownTitle = {
        brand: "",
        platform: ""
    };

    vm.brandList = [
        {id: 1, title: "Nike"},
        {id: 2, title: "Converse"}
    ];

    vm.platformList = [
        {id: 1, title: "Facebook"},
        {id: 2, title: "Twitter"}
    ];

    vm.productList = [
        {id: 1, title: "Flyknit"},
        {id: 2, title: "Jordan"},
        {id: 3, title: "Apparel"}
    ];

    vm.countryList = [
        {id: 1, title: "Netherlands"},
        {id: 2, title: "France"},
        {id: 3, title: "Ukraine"}
    ];

    vm.markOfContentList = [
        {id: 1, title: "Infringing"},
        {id: 2, title: "Genuine"},
        {id: 3, title: "Impersonating"}
    ];

    vm.infringementTypeList = [
        {id: 1, title: "Trademark"},
        {id: 2, title: "Counterfeit"},
        {id: 3, title: "Copyright"}
    ];

    vm.enforcementStatusList = [
        {id: 1, title: "Queued for enforcement,"},
        {id: 2, title: "Enforced"}
    ];

    vm.escalationStatusList = [
        {id: 1, title: "No escalation status"},
        {id: 2, title: "Escalated by Pointer"},
        {id: 3, title: "Escalated by brand"}
    ];

    vm.accountStatusList = [
        {id: 1, title: "Blocked"},
        {id: 2, title: "Cleaned"}
    ];


    vm.dropdownListener = function (item, type) {
        vm.postData[type] = item.id;
        vm.dropdownTitle[type] = item.title;
    };

    vm.submit = function () {
        RestService.submit(vm.postData);
    };

    vm.clear = function () {
        vm.postData = angular.copy(defaultValue);
    };

    vm.validateForm = function () {
        return !(vm.postData.brand && vm.postData.platform && vm.postData.markOfContent && vm.postData.infringementType
        && vm.postData.enforcementStatus && vm.postData.escalationStatus);
    };

});